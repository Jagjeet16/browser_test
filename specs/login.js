const LoginPage = require( "../pageObjects/LoginPage");

describe('Login Test Suite', function() {

    it('should login with valid credentials', async function() {
        await LoginPage.get();

        await LoginPage.login('cbtsupervisor@mailinator.com', 'Test@1234');

        await LoginPage.successfulLogin();

    });
    
    it('should NOT login with invalid credentials', async function() {
        await LoginPage.get();

        await LoginPage.login('cbtsupervisor@mailinator.com', 'Test@2234');

        await LoginPage.invalidPass();

    });
  })