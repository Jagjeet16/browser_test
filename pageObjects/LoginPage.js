const config = require('../config/qa');

const LoginPage = function() {
    const emailInput = element(by.model('loginItems.userName'));
    const passwordInput = element(by.model('loginItems.userPassword'));
    const loginBtn = element(by.buttonText('Log in'));
  
    this.get = async () => {
      await browser.get(config.baseUrl);
    };
    
    this.login = async (email, password) => {
      await emailInput.sendKeys(email);
      await passwordInput.sendKeys(password);
      await loginBtn.click();
    };

    this.successfulLogin = async () => {
      var errorMessage = protractor.ExpectedConditions;
      browser.wait(errorMessage.textToBePresentInElement($('.cls-txxt.cls-18-400.cls-m-left-10'), 'Supervisor Cbt'), 10000);
    };

    this.invalidPass = async () => {
      var errorMessage = protractor.ExpectedConditions;
      browser.wait(errorMessage.textToBePresentInElement($('.cls-login-error.ng-binding'), 'The user name or password is incorrect'), 10000);
    };
  };
  module.exports = new LoginPage();